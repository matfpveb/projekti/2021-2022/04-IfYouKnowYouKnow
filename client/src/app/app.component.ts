import { Component, ElementRef, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { User } from './models/user.model';
import { AuthService } from './services/auth.service';

declare const $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'game';

  // refresh btn - play again
  onPlayAgain(){
    location.reload();
  }

  onBackFromGame() {
    location.reload();
  }

  currentGame : string = "Jigsaw";
  showCurrGame : boolean = true;

  // to control starting timers in components
  shouldStartMatchPairs : boolean = false;
  shouldStartIfYouKnow : boolean = false;
  shouldStartAssociations : boolean = false;

  tabIndex : number = 0;
  showNextTab(showNext : boolean) {
    if (showNext)  {
      switch (this.tabIndex) {
        case 0:
          $.tab('change tab', 'jumper');
          this.currentGame = "Jumper";
          this.tabIndex++;
          break;
        case 1:
          $.tab('change tab', 'match-pairs');
          this.shouldStartMatchPairs = true;
          this.currentGame = "Match pairs";
          this.tabIndex++;
          break;  
        case 2:
          $.tab('change tab', 'if-you-know');
          this.shouldStartIfYouKnow = true;
          this.currentGame = "If you know";
          this.tabIndex++;
          break;
        case 3:
          $.tab('change tab', 'associations');
          this.shouldStartAssociations = true;
          this.currentGame = "Associations";
          this.tabIndex++;
          break;
        default:
          $.tab('change tab', 'points-won');
          this.showCurrGame = false;
          this.savePoints();
          break;        
      }
    }
  }

  totalPoints : number = 0;
  // points per game
  jigsawPoints : number = 0;
  jumperPoints : number = 0;
  matchPairsPoints : number = 0;
  ifYouKnowPoints : number = 0;
  associationsPoints : number = 0;
  
  onJigsawPointsWon(points : number) {
    this.jigsawPoints = points;
    this.totalPoints += points;
  }
  onJumperPointsWon(points : number) {
    this.jumperPoints = points;
    this.totalPoints += points;
  }
  onMatchPairsPointsWon(points : number){
    this.matchPairsPoints = points;
    this.totalPoints += points;
  }
  onIfYouKnowPointsWon(points : number){
    this.ifYouKnowPoints = points;
    this.totalPoints += points;
  }
  onAssociationsPointsWon(points : number) {
    this.associationsPoints = points;
    this.totalPoints += points;
  }
  

  @ViewChild('home') homeEl : ElementRef = {} as ElementRef;
  @ViewChild('login') loginEl : ElementRef = {} as ElementRef;

  currElement: HTMLElement;

  sub: Subscription = new Subscription()
  user: User | null = null


  constructor(private auth: AuthService){
    this.auth.user.subscribe(
      (user: User | null) => {
        this.user = user
        console.log(user)
      }
    )
  }

  startGame(){
    console.log("start");
  }

  scroll(el: HTMLElement){
    el.scrollIntoView({behavior:'smooth'})
    this.currElement = el;
  }

  public savePoints(){
    if (this.user != null){
      const obs: Observable<User | null> = this.auth.addGame(this.totalPoints)
      obs.subscribe( (user:User | null) => {
        console.log(user);
      })
    }
  }

  ngOnInit(){
    $.tab();
    $('.menu .browse').popup(
      {
        on:'click',
      }
    );

    setTimeout(()=> {this.homeEl.nativeElement.scrollIntoView({behavior:'smooth'}),
    this.currElement = this.homeEl.nativeElement;
  }, 10)
  }

  setCurrentElement(el: HTMLElement){
    this.currElement = el;
  }

}
