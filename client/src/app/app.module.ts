import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JigsawComponent } from './components/jigsaw/jigsaw.component';
import { JumperComponent } from './components/jumper/jumper.component';
import { MatchPairsComponent } from './components/match-pairs/match-pairs.component';
import { StepByStepComponent } from './components/step-by-step/step-by-step.component';
import { IfYouKnowComponent } from './components/if-you-know/if-you-know.component';
import { AssociationsComponent } from './components/associations/associations.component';
import { FindMyNumberComponent } from './components/find-my-number/find-my-number.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';

@NgModule({
  declarations: [
    AppComponent,
    JigsawComponent,
    JumperComponent,
    MatchPairsComponent,
    StepByStepComponent,
    IfYouKnowComponent,
    AssociationsComponent,
    FindMyNumberComponent,
    ProgressBarComponent,
    LoginComponent,
    SignUpComponent,
    ProfilePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
