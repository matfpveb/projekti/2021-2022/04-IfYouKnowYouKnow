export class Question {
    parser = new DOMParser();
    decode(s:string ):string {
        var ret = this.parser.parseFromString(s, "text/html").documentElement.textContent;
        return ret == null ? "" : ret
    }

    public correctNo: number;
    public answers :string[] = [];
    public question;
    constructor(question:string, correct:string, wrong:string[], public order:number){
        this.correctNo = Math.floor(Math.random() * 4);
        this.question = order + ". " + this.decode(question);
        for (var i=0; i<4; i++){
            if(i === this.correctNo){
                this.answers.push(this.decode(correct))
            } 
            else {
                var w: string | undefined = wrong.pop()
                if (w != undefined)
                    this.answers.push(this.decode(w));
            }
        }
    }
}


export class Questions {
    private httpGet(url:string) {
        return fetch(url).then(function (response) {
            return response.json();
    })};


    private url: string = "https://opentdb.com/api.php?amount=10&type=multiple"
    public questions: Question[];
    public currQuestion: undefined | Question;


    constructor(){
        this.questions = [];
        this.httpGet(this.url).then((response) => {
            // console.log(response)
            var i = 10;
            for (const question of response.results) {
                // console.log(question)
                
                this.questions.push(new Question(question.question, question.correct_answer, question.incorrect_answers, i--));
            }
        })
        .catch((err) => {
            console.log("Unable to get questions!");
            console.log(err);
        }) ;
    }

    getNextQuestion() {
        return this.currQuestion = this.questions.pop()
    }
}
