import { Game } from "./user.model";

export interface IJwtTokenData {
    username: string,
    email:string,
    password:string,
    games: Game[]
}