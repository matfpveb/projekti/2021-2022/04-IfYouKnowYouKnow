export class Game {
    constructor(
        public points:Number,
        public date: string,
        // public _id: string
    ){}
}

export class User {
    constructor(
      public username:string,
      public email: string,
      public password: string,
      public games: Game[]
    ){}
}