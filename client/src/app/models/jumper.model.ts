
export class Sign {
    JUMPER: 0;
    DIAMONDS: 1;
    ROCKET: 2;
    MOON: 3;
    STAR: 4;
    HEARTS: 5;

    signof(number: number){
            switch(number){
                case 0: 
                return 'JUMPER';
                case 1: 
                return 'DIAMONDS';
                case 2: 
                return 'ROCKET';
                case 3: 
                return 'MOON';
                case 4: 
                return 'STAR';
                default: 
                return 'HEARTS';
            }
        }
    };

export class JumperGame{

    Sign: Sign = new Sign();

    constructor(){
    };


    genAllVars(signs: any){
        const all = new Array();
        let c = [0,0,0,0];
        let n = signs.length-1;
        while(JSON.stringify(c) !== JSON.stringify([5,5,5,6])){
            // console.log(c);
            all.push([c[0], c[1], c[2], c[3]]);
            let k = c.length;
            let i = k-1;
            while (i >= 0 && c[i] === n){
                c[i] = 0;
                i--;
            }
            if(i < 0){
                break;
            }
            c[i]++;
            
        }
        return all;
    };

    getRandomVar(){
        let allVars = this.genAllVars([this.Sign.MOON, this.Sign.DIAMONDS, this.Sign.HEARTS, this.Sign.JUMPER, this.Sign.ROCKET, this.Sign.STAR]);
        let rnd = Math.floor(Math.random() * allVars.length);
        return allVars[rnd];
    };



    checkVar(randomVar : Array<number>, tryVar : Array<number>){
        let red = 0;
        let yellow = 0;
        let arr : Array<number> = new Array<number>(2);
        let rndVar : Array<number> = new Array<number>();
        let trVar : Array<number> = new Array<number>();


        for (let i=0; i<randomVar.length; i++){
            if (randomVar[i] == tryVar[i]){
                red++;
            } else {
                rndVar.push(randomVar[i]);
                trVar.push(tryVar[i]);
            }
        }
        
        rndVar.sort();
        trVar.sort();

        for (let i=0; i<trVar.length; i++){
            for(let j=0; j<rndVar.length; j++){
                if(trVar[i] == rndVar[j]) {
                    yellow++;
                    rndVar[j] = -1;
                    break;
                } 
            }
        }

        arr[0] = red;
        arr[1] = yellow;

        return arr;
    };
};


