'use strict';

export class Word {
  constructor(public _id: string, public length: number) {}
}

// usually, between every consonant there is a vowel, because of that I need to increase the chance that a vowel will be randomly chosen because there is only 5 vowel compared to 25 consonants
// this was a previous letter rng implementation, scrool down for currently used one

//const availableSymbols: String[] = 'abcdefghijklmnopqrstuvwxyz'.split('');
//
//const vowels: String[] = 'aeiou'.split('');
//const consonants: String[] = 'bcdfghjklmnpqrstvwxyz'.split('');

// https://www3.nd.edu/~busiforc/handouts/cryptography/letterfrequencies.html

// this is the most frequent letter - 11.1% (4% chance that a letter from this group will be selected - we increase it to 11% chance)
const letterE: string = 'e';
// 8.5%
const letterA: string = 'a';
// 7.5%
const letterR: string = 'r';
// 7.5%
const letterI: string = 'i';
// 7.1%
const letterO: string = 'o';
// 6.9%
const letterT: string = 't';
// 6.6%
const letterN: string = 'n';
// 5.7%
const letterS: string = 's';
// 5.4%
const letterL: string = 'l';

const restOfTheAlphabet: string[] = 'bcdfghjkmpquvwxyz'.split('');

export const getRandomLetter = () => {
  const diceRoll = Math.random();

  // scaled a bit
  if (diceRoll <= 0.08) {
    return letterE;
  } else if (diceRoll <= 0.15) {
    return letterA;
  } else if (diceRoll <= 0.21) {
    return letterR;
  } else if (diceRoll <= 0.27) {
    return letterI;
  } else if (diceRoll <= 0.326) {
    return letterO;
  } else if (diceRoll <= 0.38) {
    return letterT;
  } else if (diceRoll <= 0.431) {
    return letterN;
  } else if (diceRoll <= 0.473) {
    return letterS;
  } else if (diceRoll <= 0.513) {
    return letterL;
  } else {
    return restOfTheAlphabet[
      Math.floor(Math.random() * restOfTheAlphabet.length)
    ];
  }
  //if (Math.random() <= 0.36) {
  //  return vowels[Math.floor(Math.random() * vowels.length)];
  //} else {
  //  return consonants[Math.floor(Math.random() * consonants.length)];
  //}
};

export function calculatePoints(
  userWordSize: number,
  computerWordSize: number
): number {
  return userWordSize === computerWordSize && userWordSize !== 0
    ? userWordSize * 2 + 5
    : userWordSize * 2;
}
