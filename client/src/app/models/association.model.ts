export class AssociationColumn {
    constructor(public r1: AssociationField, public r2: AssociationField, 
                public r3: AssociationField, public r4: AssociationField, public solution: SolutionField) 
    {}

    public revealed: boolean = false;
    public solved: boolean = false;

    checkSolution(event: Event) {
        if(this.revealed)
            return;

        const userSolution = (event.target as HTMLInputElement).value;

        if(userSolution.toLowerCase() == this.solution.answer) {
            this.solveColumn();
        }
    }

    calculatePoints() : number {
        let points: number = 0;

        if(!this.solved)
            return points;

        points += 6;
        points += this.r1.calculatePoints();
        points += this.r2.calculatePoints();
        points += this.r3.calculatePoints();
        points += this.r4.calculatePoints();

        return points;
    }

    revealColumn() {
        if(this.revealed)
            return;

        this.calculatePoints();

        this.solution.text = this.solution.answer;
        this.solution.field_class = "ui disabled fluid input";
        this.solution.answered = true;
        this.revealed = true;

        this.r1.reveal();
        this.r2.reveal();
        this.r3.reveal();
        this.r4.reveal();
    }

    solveColumn() {
        if(this.revealed)
            return;

        this.solved = true;
        this.revealColumn();
    }
}

export class SolutionField {
    constructor(public text: string, public answer: string, public field_class: string, public answered: boolean) {

    }
}

export class AssociationField {
    constructor(public text: string, public answer: string, public field_class: string) {
        
    }

    public revealed: boolean = false;
    public clicked: boolean = false;
    public points: number = 2;

    reveal() {
        if(this.revealed)
            return;

        this.revealed = true;
        this.text = this.answer;
        this.field_class = "green ui button";
    }

    calculatePoints() : number {
        if(this.clicked)
            return 0;
        else
            return 1;
    }

    onFieldClick() {
        this.clicked = true;
        this.calculatePoints();
        this.reveal();
    }
}