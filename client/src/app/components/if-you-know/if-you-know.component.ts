import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Question, Questions } from 'src/app/models/question.model';

declare const $:any;

@Component({
  selector: 'app-if-you-know',
  templateUrl: './if-you-know.component.html',
  styleUrls: ['./if-you-know.component.css']
})
export class IfYouKnowComponent implements OnInit {

  @Output() sendPointsWon : EventEmitter<number> = new EventEmitter<number>();
  pointsWon : number = 0;

  public questions: Questions;
  public currQuestion: Question;
  neutralBtnStyle: string = "fluid ui primary basic button";
  negativeBtnStyle: string = "fluid ui primary negative button disabled";
  positiveBtnStyle: string = "fluid ui primary positive button disabled";
  answers: string[];

  // nextQuestionTimeout = setTimeout(()=>{}, 0);
  questionTime = setInterval(()=>{}, 0)

  oneQuestionTime:number = 8000;
  intervalDuration = 500;
  time:number = 0;

  gameOver:boolean = false;

  constructor() {
    this.questions = new Questions()
    this.currQuestion = new Question("", "", ["", "", ""], -1);
    this.answers = [this.neutralBtnStyle, this.neutralBtnStyle, this.neutralBtnStyle, this.neutralBtnStyle];
  }


  public setNextQuestion () {
    var q = this.questions.getNextQuestion();
    if (q == undefined){
      this.gameOver = true;
      // TODO next game
      return;
    }

    for (let i in this.answers)
      this.answers[i] = this.neutralBtnStyle;

    this.currQuestion = q

    // clearTimeout(this.nextQuestionTimeout); // clear timer before next question
    clearInterval(this.questionTime); // clear interval before next question
    this.time = 0;

    // PROGRESS BAR
    $("#progressBar").progress({total:this.oneQuestionTime, value: 0})
    this.questionTime = setInterval(()=> {
      if (this.time == this.oneQuestionTime){
        this.markAnswers()
        clearInterval(this.questionTime)
      }

      this.time = this.time + this.intervalDuration;
      $("#progressBar").progress({total:this.oneQuestionTime, value:this.time})
        }
        , this.intervalDuration);

    // this.nextQuestionTimeout = setTimeout(() => {
        // this.markAnswers();
    // }, this.oneQuestionTime);

  }

  markAnswers(){
    for (let i in this.answers)
      this.answers[i] = this.negativeBtnStyle;
    this.answers[this.currQuestion.correctNo] = this.positiveBtnStyle;
    setTimeout(()=> this.setNextQuestion(), 2000); // mark correct and wrong, go to next qusetion in 3 seconds
  }

  checkAnswer(event: Event){
    // console.log(event);
    // clearTimeout(this.nextQuestionTimeout); // stops current timer
    clearInterval(this.questionTime)
    this.time = 0
    this.markAnswers();

    // TODO poeni
    if ((event.target as HTMLButtonElement).id == this.currQuestion.correctNo + ""){
      // console.log("correct")
      this.pointsWon += 10;
    }
    else {
      console.log("wrong")
      this.pointsWon -= 5;
    }

  }

  @Input() shouldStartTimer : boolean;
  ngOnInit(): void {
    this.shouldStartTimer = false;
    // setTimeout(() => {
    //   this.setNextQuestion()}, 1000)
  }

  ngOnChanges() {
    if (this.shouldStartTimer) {
      setTimeout(() => {
        this.setNextQuestion()}, 0)
    }
  }

  @Output() showNextGame : EventEmitter<boolean> = new EventEmitter<boolean>();
  public nextGame(){
    this.sendPointsWon.emit(this.pointsWon);
    this.showNextGame.emit(true);
  }
}
