import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IfYouKnowComponent } from './if-you-know.component';

describe('IfYouKnowComponent', () => {
  let component: IfYouKnowComponent;
  let fixture: ComponentFixture<IfYouKnowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IfYouKnowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IfYouKnowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
