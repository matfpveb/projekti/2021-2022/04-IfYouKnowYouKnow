import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { EmailValidator, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UserPasswordValdator } from 'src/app/validators/user-password-validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit, OnDestroy {
  sub: Subscription = new Subscription();

  userForm: FormGroup;

  @Input()
  homeDiv: HTMLElement 

  constructor(private auth: AuthService) { 
    this.userForm = new FormGroup({
      username: new FormControl("", [Validators.required, Validators.minLength(5), Validators.pattern(RegExp("^[A-Za-z0-9_-]+$"))]),
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.minLength(8), UserPasswordValdator])
    })
  }

  @Output() scrolledBack : EventEmitter<HTMLElement> = new EventEmitter<HTMLElement>();

  saveNewUser(){
    const data = this.userForm.value;
    console.log(data)

    if (this.userForm.invalid){
      window.alert("Some fields are not valid.");
      return;
    }    

    const obs: Observable<User | null> = this.auth.registerUser(data.username, data.password, data.email)

    this.sub = obs.subscribe(
        (user:User | null) => { console.log(user);     
        this.homeDiv.scrollIntoView({behavior:'smooth'});
      // set current view for zoom problem
          this.scrolledBack.emit(this.homeDiv)

      },
      error => alert(error.error.message)
    )
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
      this.sub.unsubscribe();
  }

  passwordHasErrors(): boolean{
    return this.userForm.get("password")?.errors != null;
  }

  passwordErrors(): string[]{
    const errors: ValidationErrors | null | undefined = this.userForm.get("password")?.errors;

    if (errors==null)
      return []

    const messages: string[] = []

    if(errors.required)
      messages.push("This field is required.");

    if (errors.minlength)
      messages.push("Password must have at least 8 characters.")

      // console.log(errors)
    if (errors.userPassword){
      messages.push(errors.userPassword)
    }

    return messages
  }

  usernameHasErrors(): boolean{
    return this.userForm.get("username")?.errors != null;
  }

  usernameErrors(): string[]{
    const errors: ValidationErrors | null | undefined = this.userForm.get("username")?.errors;

    if (errors==null)
      return []

    const messages: string[] = []

    if(errors.required)
      messages.push("This field is required.");

    if (errors.minlength)
      messages.push("Username must have at least 5 characters.")
    
    if (errors.pattern)
      messages.push("Username can contain only letters, numbers, _ and -.")
    
    return messages
  }

  emailHasErrors(): boolean{
    return this.userForm.get("email")?.errors != null;
  }

  emailErrors(): string[]{
    const errors: ValidationErrors | null | undefined = this.userForm.get("email")?.errors;

    if (errors==null)
      return []

    const messages: string[] = []

    if(errors.required)
      messages.push("This field is required.");

    if (errors.email)
      messages.push("Email must be valid.")
    
    return messages
  }
}
