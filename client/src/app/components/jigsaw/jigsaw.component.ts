import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Word,
  getRandomLetter,
  calculatePoints
} from 'src/app/models/jigsaw.model';
import { ProgressBarComponent } from '../progress-bar/progress-bar.component';
import { WordsService } from 'src/app/services/WordsService';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-jigsaw',
  templateUrl: './jigsaw.component.html',
  styleUrls: ['./jigsaw.component.css'],
})
export class JigsawComponent implements OnInit, AfterViewInit, OnDestroy {
  getRandomLetter = getRandomLetter;
  letterButtonClass: string = 'ui huge teal button';
  generateLettersClass: string = 'large ui positive button';
  submitWordClass: string = 'black big ui neutral button';
  hiddenWordCheckLabelClass: string = 'ui pointing left big green basic label';

  progressBar: ProgressBarComponent = new ProgressBarComponent();
  private activeSubscriptions: Subscription[] = [];

  @ViewChild('lb1')
  lb1!: ElementRef;
  @ViewChild('lb2')
  lb2!: ElementRef;
  @ViewChild('lb3')
  lb3!: ElementRef;
  @ViewChild('lb4')
  lb4!: ElementRef;
  @ViewChild('lb5')
  lb5!: ElementRef;
  @ViewChild('lb6')
  lb6!: ElementRef;
  @ViewChild('lb7')
  lb7!: ElementRef;
  @ViewChild('lb8')
  lb8!: ElementRef;
  @ViewChild('lb9')
  lb9!: ElementRef;
  @ViewChild('lb10')
  lb10!: ElementRef;
  @ViewChild('lb11')
  lb11!: ElementRef;
  @ViewChild('lb12')
  lb12!: ElementRef;
  letterButtonIDs: ElementRef[] = [];

  // FOR SOME BLOODY REASON ADDING ELEMENTS BY HAND DOES NOW WORK
  // I KNOW IT'S AWFUL BUT I JUST NEED TO MOVE ON WITH MY LIFE AT THIS POINT
  addChildren() {
    this.letterButtonIDs.push(this.lb1);
    this.letterButtonIDs.push(this.lb2);
    this.letterButtonIDs.push(this.lb3);
    this.letterButtonIDs.push(this.lb4);
    this.letterButtonIDs.push(this.lb5);
    this.letterButtonIDs.push(this.lb6);
    this.letterButtonIDs.push(this.lb7);
    this.letterButtonIDs.push(this.lb8);
    this.letterButtonIDs.push(this.lb9);
    this.letterButtonIDs.push(this.lb10);
    this.letterButtonIDs.push(this.lb11);
    this.letterButtonIDs.push(this.lb12);
  }

  submitWordDisabled: boolean = true;
  nextGameDisabled: boolean = true;
  genLetDisabled: boolean = false;
  letterButtonDisabled: boolean = true;
  deleteLetterDisabled: boolean = true;

  selectedLetters: string = '';
  hiddenMessage: string = "Click 'Generate letters' to start";
  playerPoints: number = 0;
  @Output() sendPointsWon: EventEmitter<number> = new EventEmitter<number>();
  letterValue: string = '';
  timer: number = 5.0;
  foundWord: string = '';
  foundWordSize: number = 0;
  computerWord: string = '';
  computerWordSize: number = 0;
  tmpComputerWord: string = 'searching for a word...';

  public constructor(private wordsService: WordsService) {}

  generateLetters(): void {
    this.generateLettersSetFlags();

    for (let lb of this.letterButtonIDs) {
      const letter = getRandomLetter();
      this.selectedLetters += letter;
      lb.nativeElement.innerHTML = letter;
      lb.nativeElement.value = letter;
    }

    this.progressBar.startTimer(this.progressBar.jigsawTimer, this);
    this.generateLongestWord();
  }

  private restoreLetter(letter: string): void {
    for (let lb of this.letterButtonIDs) {
      const button = lb.nativeElement;
      if (button.value === letter && button.disabled === true) {
        button.disabled = false;
        button.className = this.letterButtonClass;
        break;
      }
    }
  }

  removeLetterClick(): void {
    const len: number = this.foundWord.length;
    if (len !== 0) {
      const lastLetter = this.foundWord.charAt(len - 1);
      this.foundWordSize -= 1;
      this.foundWord = this.foundWord.slice(0, -1);
      this.restoreLetter(lastLetter);
    }
    if (len - 1 <= 0) {
      this.deleteLetterDisabled = true;
    }
  }

  letterButtonClick(button: any) {
    button.className = 'ui large black circular button';
    button.disabled = true;
    this.foundWord += button.value;
    this.foundWordSize += 1;
    this.deleteLetterDisabled = false;
  }

  submitWord(): void {
    this.progressBar.stopTimer();
    this.sumbitWordSetFlags();

    // computer did not have time to find a longest word so it sent another request for it (async/await should be the proper way of doing this)
    if (this.tmpComputerWord !== "searching for a word...") {
      this.computerWord = this.tmpComputerWord;
      this.computerWordSize = this.computerWord.length;
      this.checkUserWord();
    }
    else {
      this.hiddenMessage = 'Waiting for computer...';
      this.hiddenWordCheckLabelClass = 'ui pointing left big yellow basic label';
      this.computerWord = this.tmpComputerWord;
      this.generateLongestWord(true);
    }
  }

  private generateLongestWord(playerDone: boolean = false) {
    const sub = this.wordsService
      .findLongestWord(this.selectedLetters)
      .subscribe((response) => {
        // checks to see if player submited word before computer could find its
        if (!playerDone) {
          this.tmpComputerWord = response['_id'];
        }
        else {
          this.computerWord = response['_id'];
          this.computerWordSize = response['length'];
          this.checkUserWord();
        }
      });

    this.activeSubscriptions.push(sub);
  }

  private checkUserWord() {
    const sub = this.wordsService
      .checkWord(<string>this.foundWord)
      .subscribe((response) => {
        if (response['length'] > 0) {
          this.setWordExistsStatus();
        } else {
          this.setWordDoesNotExistStatus();
        }
      });

    this.activeSubscriptions.push(sub);
    this.nextGameDisabled = false;
  }

  private setWordExistsStatus() {
    if (this.foundWordSize === this.computerWordSize) {
      this.hiddenMessage = 'Word EXISTS! You get BONUS points!';
      this.submitWordClass = 'ui large teal button';
      this.hiddenWordCheckLabelClass = 'ui pointing left big teal basic label';
    } else {
      this.submitWordClass = 'ui large positive button';
      this.hiddenMessage = 'Word EXISTS!';
      this.hiddenWordCheckLabelClass = 'ui pointing left big green basic label';
    }

    this.playerPoints = calculatePoints(
      this.foundWordSize,
      this.computerWordSize
    );
    // this.sendPointsWon.emit(this.playerPoints);
  }

  private setWordDoesNotExistStatus() {
    this.hiddenMessage = 'We could not find your word :(';
    this.submitWordClass = 'ui large negative button';
    this.hiddenWordCheckLabelClass = 'ui pointing left big red basic label';

    this.playerPoints = calculatePoints(0, this.computerWordSize);
    // this.sendPointsWon.emit(this.playerPoints);
  }

  noMoreTime() {
    // here you handle all your end game stuff
    this.submitWord();
  }

  private sumbitWordSetFlags() {
    this.submitWordDisabled = true;
    this.letterButtonDisabled = true;
    this.deleteLetterDisabled = true;
  }

  private generateLettersSetFlags() {
    this.generateLettersClass = 'large ui neutral button';
    this.genLetDisabled = true;
    this.submitWordDisabled = false;
    this.letterButtonDisabled = false;
    this.addChildren();
  }

  ngAfterViewInit(): void {}
  ngOnInit(): void {}
  ngOnDestroy() {
    this.activeSubscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }
  @Output() showNextGame: EventEmitter<boolean> = new EventEmitter<boolean>();
  onNextGame() {
    this.sendPointsWon.emit(this.playerPoints);
    this.showNextGame.emit(true);
  }
}
