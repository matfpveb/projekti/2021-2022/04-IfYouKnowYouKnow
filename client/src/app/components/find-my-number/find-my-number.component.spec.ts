import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindMyNumberComponent } from './find-my-number.component';

describe('FindMyNumberComponent', () => {
  let component: FindMyNumberComponent;
  let fixture: ComponentFixture<FindMyNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindMyNumberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindMyNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
