import { Component, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';

import { LogService } from 'src/app/log.service';
declare const $: any;

import { JumperGame, Sign } from 'src/app/models/jumper.model';

enum Try {
  FIRST = 3,
  SECOND = 7,
  THIRD = 11,
  FOURTH = 15,
  FIFTH = 19,
  SIXTH = 23
}


@Component({
  selector: 'app-jumper',
  templateUrl: './jumper.component.html',
  styleUrls: ['./jumper.component.css']
})
export class JumperComponent implements OnInit, AfterViewInit {

  // progress bar

  interval : any;
  jumperTimer: any = { value: 100.0 };

  // jumper game 

  game : JumperGame = new JumperGame();
  randomVar : Array<number> = this.game.getRandomVar();
  sign : Sign = new Sign();

  gameOver : boolean = false;

  @Output() showNextGame : EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() sendPointsWon : EventEmitter<number> = new EventEmitter<number>();
  pointsWon : number;

  numOfButtons : number = 48;
  numOfClicks : Array<number> = new Array<number>(this.numOfButtons);
  iconClass : Array<string> = new Array<string>(this.numOfButtons);
  shouldBeDisabled : Array<boolean> = new Array<boolean>(this.numOfButtons);

  tryVar : Array<number> = new Array<number>(4);
  numOfRedCircles : number;
  numOfYellowCircles : number;
  lastEnabled : number;

  showJumperHelp : boolean = true;

  constructor() {
  }

  initializeProgressBar(timer: any) {
    const value = timer["value"] * 1000;
    $("#jumperProgressBarID").progress({total: value, value});
  }
  stopTimer(){
    clearInterval(this.interval)
  }
  startTimer(timer: any, component: any) {
    this.initializeProgressBar(timer);

    const refreshIntervalMs = 100;

    this.interval = setInterval(function () {
      $("#jumperProgressBarID").progress('decrement', refreshIntervalMs);
      if (timer['value'] <= 0.0) {
        $("#jumperProgressBarID").progress({percent: 0});
        component.endGame();
        return;
      }

      timer['value'] -= refreshIntervalMs / 1000;
      LogService.prototype.log(timer['value']);
    }, refreshIntervalMs);
  }

  ngOnInit(): void {
    $("#jumperProgressBarID").progress({percent: 100});
    this.numOfRedCircles = 0;
    this.numOfYellowCircles = 0;
    for (let i=0; i<this.numOfButtons; i++){
      this.iconClass[i] = "large teal circle outline icon";
      if (i >= 4) {
        this.shouldBeDisabled[i] = true;
      }
      else {
        this.shouldBeDisabled[i] = false;
      }
      this.numOfClicks[i] = -1;
    }
    this.lastEnabled = Try.FIRST;
    this.pointsWon = 0;

  }


  varIconClass(sign : number){
    if (!this.gameOver) {
      return "large teal circle outline icon";
    }
    switch(sign){
      case 0:
        return "large red child icon";
      case 1:
        return "large teal diamond icon";
      case 2:
        return "large black rocket icon";
      case 3:
        return "large teal moon icon";
      case 4:
        return "large yellow star icon";
      default:
        return "large red heart icon";
    }
  }

  onSignClick(id : number) {
    if (id < 24) { 
      this.numOfClicks[id]++;
      let remainder = this.numOfClicks[id]%6;
      switch(remainder){
        case 0:
          this.iconClass[id] = "large red child icon";
          break;
        case 1:
          this.iconClass[id] = "large teal diamond icon";
          break;
        case 2:
          this.iconClass[id] = "large black rocket icon";
          break;
        case 3:
          this.iconClass[id] = "large teal moon icon";
          break;
        case 4:
          this.iconClass[id] = "large yellow star icon";
          break;  
        case 5:
          this.iconClass[id] =  "large red heart icon";
          break;
      }
    }
  }

  disableButtons() {
    for (let j=0; j<this.numOfButtons/2; j++) {
        this.shouldBeDisabled[j] = true;
    }
  }

  endGame() {
    this.gameOver = true;
    this.disableButtons();
    switch(this.lastEnabled) {
      case Try.FIRST:
      case Try.SECOND:
        if (this.numOfRedCircles == 4) {
          this.pointsWon = 20; 
        }
        break;
      case Try.THIRD:
      case Try.FOURTH:
        if (this.numOfRedCircles == 4) {
          this.pointsWon = 15; 
        }
        break;
      case Try.FIFTH:
      case Try.SIXTH:
        if (this.numOfRedCircles == 4) {
          this.pointsWon = 10; 
        } 
        break;   
      default:
        this.pointsWon = 0;
        break;
    }
  }
  

  onCheck(){
    let noSign : boolean = false;
    for (let i=this.lastEnabled; i>=0 && this.shouldBeDisabled[i] == false; i--){
      if (this.numOfClicks[i] == -1){
        noSign = true;
        break;
      }
    }

    if (!noSign) {
      this.showJumperHelp = false;
      if (this.lastEnabled == Try.FIRST) {
        this.startTimer(this.jumperTimer, this);
      }

      let numOfRedCircles = 0;
      let numOfYellowCircles = 0;
      let variation : Array<number> = new Array<number>(4);
      for (let i=this.lastEnabled; i>=0 && this.shouldBeDisabled[i] == false; i--){
        variation[i%4] = this.numOfClicks[i]%6;
      }
      this.tryVar = variation;

      const numCircles : Array<number> = this.game.checkVar(this.randomVar, this.tryVar);

      numOfRedCircles = numCircles[0];
      numOfYellowCircles = numCircles[1];

      this.numOfRedCircles = numOfRedCircles;
      this.numOfYellowCircles = numOfYellowCircles;

      for (let i=this.lastEnabled; i>=0 && this.shouldBeDisabled[i] == false; i--){
        if (!this.gameOver) {
          this.shouldBeDisabled[i+4] = false;
        }
        this.shouldBeDisabled[i+this.numOfButtons/2] = false;
        this.shouldBeDisabled[i] = true;
      }
      let i=this.lastEnabled + this.numOfButtons/2 - 4;
      while(numOfRedCircles > 0 && i<=this.lastEnabled + this.numOfButtons/2) {
        numOfRedCircles--;
        i++;
        this.iconClass[i] = "large red circle icon";
      }
      while(numOfYellowCircles > 0 && i<=this.lastEnabled + this.numOfButtons/2) {
        numOfYellowCircles--;
        i++;
        this.iconClass[i] = "large yellow circle icon";
      }
      
      if (this.lastEnabled == Try.SIXTH || this.numOfRedCircles == 4) {
        this.stopTimer();
        this.endGame();
      } else {
        this.lastEnabled += 4;
      }
      
    }
  }

  onNextGame() {
    this.sendPointsWon.emit(this.pointsWon);
    this.showNextGame.emit(true);
  }

  ngAfterViewInit(): void {
  }

}
