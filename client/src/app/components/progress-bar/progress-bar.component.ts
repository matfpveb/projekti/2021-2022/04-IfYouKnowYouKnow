import {
  AfterViewInit,
  Component,
  OnInit,
} from '@angular/core';
import { LogService } from 'src/app/log.service';

declare const $: any;

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css'],
})
export class ProgressBarComponent implements OnInit, AfterViewInit {

  interval:any;

  //place your timers here
  jigsawTimer: number = 90.0;
  matchPairTimer: number = 10.0;

  constructor() {

  }

  private initializeProgressBar(timer: number) {
    const value = timer * 1000;
    $("#progressBarID").progress({total: value, value});
  }

  public stopTimer(){
    clearInterval(this.interval)
  }

  public startTimer(timer: number, component: any) {
    $('#progressBarID').progress({ percent: 100 });
    this.initializeProgressBar(timer);

    const refreshIntervalMs = 100;

    // decrements your timer for 0.1s every 0.1s
    // not the most accurate timer because we do not have control of when exactly will this function be called but it is accurate enough

    // this will iterate 1 time extra but for the sake of readability I will leave it to '0.0' not '0.1' like it ought to be
    this.interval = setInterval(function () {
      $("#progressBarID").progress('decrement', refreshIntervalMs);
      if (timer <= 0.0) {
        $('#progressBarID').progress({percent: 0});
        component.noMoreTime();
        return;
      }

      timer -= refreshIntervalMs / 1000;
      //LogService.prototype.log(timer);
    }, refreshIntervalMs);
  }

  ngOnInit(): void {}
  ngAfterViewInit(): void {
    $("#progressBarID").progress({percent: 100});
  }
}
