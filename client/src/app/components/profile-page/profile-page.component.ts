import { Component, Input, OnDestroy, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { data } from 'jquery';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit, OnDestroy {

  @Input()
  homeDiv: HTMLElement

  sub: Subscription = new Subscription()
  loginBtnString: String = "LOGIN"
  user: User = new User("", "", "", [])


  constructor(public auth: AuthService) {
    this.sub = this.auth.user.subscribe(
      (user: User | null) => {
        if (user != null)
          this.user = user
      }
    )

    console.log(this.user)


    const u : User | null = this.auth.sendUserDataIfExists()
    if (u !== null)
      this.user = u;
  }

  ngOnInit(): void {
    console.log(this.user)
  }

  ngOnDestroy(): void {
      
  }

  @Output() scrolledBack : EventEmitter<HTMLElement> = new EventEmitter<HTMLElement>();
  scrollBack(){
    this.homeDiv.scrollIntoView({behavior:'smooth'})  
    this.scrolledBack.emit(this.homeDiv);

  }

  getTotalPoints() : Number{
    return this.user.games.map(g => g.points).reduce((acc, curr) => acc = curr.valueOf() + acc.valueOf(), 0)
  }
  averagePoints() : Number {
    if (this.user.games.length === 0)
      return 0
    return this.getTotalPoints().valueOf( ) / this.user.games.length
  }
}
