import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AssociationColumn, AssociationField, SolutionField } from 'src/app/models/association.model'
import { AssociationsService } from 'src/app/services/associations.service';

declare const $:any;

@Component({
  selector: 'app-associations',
  templateUrl: './associations.component.html',
  styleUrls: ['./associations.component.css']
})
export class AssociationsComponent implements OnInit {

  @Output() sendPointsWon : EventEmitter<number> = new EventEmitter<number>();
  pointsWon : number = 0;

  blank_field: AssociationField = new AssociationField("", "", "ui button");
  blank_solution: SolutionField = new SolutionField("", "", "ui button", false);
  blank_column: AssociationColumn = new AssociationColumn(this.blank_field, this.blank_field, this.blank_field, this.blank_field, this.blank_solution);

  column_a : AssociationColumn = this.blank_column;
  column_b : AssociationColumn = this.blank_column;
  column_c : AssociationColumn = this.blank_column;
  column_d : AssociationColumn = this.blank_column;

  allowedTime: number;
  elapsedTime: number;
  timeInterval: number;
  timer = setInterval(()=>{}, 0);

  solution : string = "Solution";
  answer : string;
  answered : boolean = false;
  solved: boolean = false;

  field_class : string = "ui big fluid input";
  end_class: string = "ui disabled button";

  constructor(private assocService: AssociationsService) { 

    this.allowedTime = 90000;
    this.elapsedTime = 0;
    this.timeInterval = 1000;

    // this.timer = setInterval(() => {
    //   this.elapsedTime += this.timeInterval; // koliko je vremena proslo
    //   var progress = this.elapsedTime / this.allowedTime * 100; // procenat koliko je vremena proslo
    //   if (progress >= 100){
    //     $("#assocProgressBar").progress({percent:100});
    //     this.Solve();
    //     clearInterval(this.timer);  
    //   }
    //   $("#assocProgressBar").progress({percent:progress});  // ocekuje procenat ispunjenosti, to se racuna iznad
    // }, this.timeInterval);
  }

  checkMainSolution(event: Event) {
    const userSolution = (event.target as HTMLInputElement).value;

        if(userSolution.toLowerCase() === this.answer) {
          this.solve();
        }
  }

  @Output() showPoints : EventEmitter<boolean> = new EventEmitter<boolean>();

  CalculatePoints() {
    if(this.solved)
      this.pointsWon += 10;

    console.log("SOLVED FINAL POINTS: " + this.pointsWon);

    this.pointsWon += this.column_a.calculatePoints();
    this.pointsWon += this.column_b.calculatePoints();
    this.pointsWon += this.column_c.calculatePoints();
    this.pointsWon += this.column_d.calculatePoints();
  }

  revealAnswer() {
    this.solution = this.answer;
    this.field_class = "ui disabled big fluid input";
    this.answered = true;
    this.enableEndButton();

    $("#assocProgressBar").progress({percent: 100});
    clearInterval(this.timer);
  }

  revealAssociation() {
    this.revealAnswer();

    this.column_a.revealColumn();
    this.column_b.revealColumn();
    this.column_c.revealColumn();
    this.column_d.revealColumn();
  }

  solve() {
    this.solved = true;
    this.revealAnswer();

    this.column_a.solveColumn();
    this.column_b.solveColumn();
    this.column_c.solveColumn();
    this.column_d.solveColumn();
  }

  @Input() shouldStartTimer : boolean;
  ngOnInit(): void {
    this.loadAssociation();
    this.shouldStartTimer = false;
    $("#assocProgressBar").progress({percent:0});
  }

  ngOnChanges() {
    if (this.shouldStartTimer) {
      this.timer = setInterval(() => {
        this.elapsedTime += this.timeInterval; // koliko je vremena proslo
        var progress = this.elapsedTime / this.allowedTime * 100; // procenat koliko je vremena proslo
        if (progress >= 100){
          $("#assocProgressBar").progress({percent:100});
          this.revealAssociation();
          clearInterval(this.timer);  
        }
        $("#assocProgressBar").progress({percent:progress});  // ocekuje procenat ispunjenosti, to se racuna iznad
      }, this.timeInterval);
    }
  }

  setFields(association: any) {

    this.column_a = new AssociationColumn(
      new AssociationField("A1", association.column_a.r1, "ui button"),
      new AssociationField("A2", association.column_a.r2, "ui button"),
      new AssociationField("A3", association.column_a.r3, "ui button"),
      new AssociationField("A4", association.column_a.r4, "ui button"),
      new SolutionField("solution A", association.column_a.solution, "ui fluid input", false)
    );

    this.column_b = new AssociationColumn(
      new AssociationField("B1", association.column_b.r1, "ui button"),
      new AssociationField("B2", association.column_b.r2, "ui button"),
      new AssociationField("B3", association.column_b.r3, "ui button"),
      new AssociationField("B4", association.column_b.r4, "ui button"),
      new SolutionField("solution B", association.column_b.solution, "ui fluid input", false)
    );

    this.column_c = new AssociationColumn(
      new AssociationField("C1", association.column_c.r1, "ui button"),
      new AssociationField("C2", association.column_c.r2, "ui button"),
      new AssociationField("C3", association.column_c.r3, "ui button"),
      new AssociationField("C4", association.column_c.r4, "ui button"),
      new SolutionField("solution C", association.column_c.solution, "ui fluid input", false)
    );

    this.column_d = new AssociationColumn(
      new AssociationField("D1", association.column_d.r1, "ui button"),
      new AssociationField("D2", association.column_d.r2, "ui button"),
      new AssociationField("D3", association.column_d.r3, "ui button"),
      new AssociationField("D4", association.column_d.r4, "ui button"),
      new SolutionField("solution D", association.column_d.solution, "ui fluid input", false)
    );

    this.answer = association.main_solution;
  }

  loadAssociation() {
    const sub = this.assocService.sendAssociation().subscribe((response) => {
      //console.log('********   ' + response + '    ********');
      this.setFields(response);
    })
  }

  enableEndButton() {
    this.end_class = "ui button";
  }

  endGame() {
    this.CalculatePoints();
    this.sendPointsWon.emit(this.pointsWon);
    this.showPoints.emit(true);
  }
}
