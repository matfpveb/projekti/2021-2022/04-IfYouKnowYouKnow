import { Component, Input, OnDestroy, OnInit, Output, EventEmitter} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup
  sub: Subscription = new Subscription()
  user: User | null = null

  @Output() scrolledToSignUp : EventEmitter<HTMLElement> = new EventEmitter<HTMLElement>();

  loginBtnString: String = "LOGIN"

  @Input()
  loginDiv: HTMLElement

  constructor(private auth: AuthService, private userServices: UserService) { 
    this.loginForm = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    })

    this.auth.user.subscribe(
      (user: User | null) => {
        this.user = user
        if (user === null)
          this.loginBtnString = "LOGIN"
        else 
          this.loginBtnString = user.username
        console.log(user)
      }
    )
    this.user = this.auth.sendUserDataIfExists()
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
      this.sub.unsubscribe();
  }


  signin(){
    const data = this.loginForm.value;
    console.log(data)

    if (this.loginForm.invalid){
      window.alert("Some fileds are not valid.");
      return;
    }    

    const obs: Observable<User | null> = this.auth.login(data.username, data.password);

    this.sub = obs.subscribe((user: User | null) => {
      // this.user = user
      // if (user === null)
      //   this.loginBtnString = "LOGIN"
      // else 
      //   this.loginBtnString = user.username
      console.log(user)
    },
      error => alert(error.error.message)
    )

    this.auth.login(data.username, data.password)
  }


  signout(){
    this.auth.logout();
    this.user = null; // TODO HACK
  };


  scrollUp(){
    this.loginDiv.scrollIntoView({behavior:'smooth'})  

    this.scrolledToSignUp.emit(this.loginDiv);
  }
}
