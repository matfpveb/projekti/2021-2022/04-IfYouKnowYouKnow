import { Component, OnInit, AfterViewInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Pair, shuffle } from 'src/app/models/pair.model';
import { LogService } from 'src/app/log.service';
import {MatchService } from 'src/app/services/MatchService';
import { Subscription } from 'rxjs';


declare const $: any;

@Component({
  selector: 'app-match-pairs',
  templateUrl: './match-pairs.component.html',
  styleUrls: ['./match-pairs.component.css']
})
export class MatchPairsComponent implements OnInit, AfterViewInit {

  @Output() sendPointsWon : EventEmitter<number> = new EventEmitter<number>();
  pointsWon : number = 0;

  //TIMER
  interval:any;
  matchPairTimer: any = { value: 60.0};

  private initializeProgressBar(timer: any) {
    const value = timer["value"] * 1000;
    $("#matchProgressBarID").progress({total: value, value});
  }

  public stopTimer(){
    clearInterval(this.interval)
  }

  public startTimer(timer: any, component: any) {
    $("#matchProgressBarID").progress({percent: 100});

    this.initializeProgressBar(timer);

    const refreshIntervalMs = 100;

    // decrements your timer for 0.1s every 0.1s
    // not the most accurate timer because we do not have control of when exactly will this function be called but it is accurate enough

    // this will iterate 1 time extra but for the sake of readability I will leave it to '0.0' not '0.1' like it ought to be
    this.interval = setInterval(function () {
      $("#matchProgressBarID").progress('decrement', refreshIntervalMs);
      if (timer['value'] <= 0.0) {
        $('#matchProgressBarID').progress({percent: 100});
        component.noMoreTime();
        return;
      }

      timer['value'] -= refreshIntervalMs / 1000;
      //LogService.prototype.log(timer['value']);
    }, refreshIntervalMs);
  }




  rightButtonsDisabled: Boolean[] = [];
  rightButtonsDisabledCheck : Boolean[] = [];
  leftButtonsDisabled: Boolean[] = [];
  leftButtonsDisabledCheck : Boolean[] = [];
  nextGameDisabled: Boolean = true;
  rightButtonClass: String = 'ui teal button';
  leftButtonClass : String[] = [];
  subject : String = '';

  pairs : any[] = [];
  firstColumn : String[] = [];
  secondColumn : String[] = [];
  leftButtonClickedId : number = 0;
  rightButtonClickedId : number = 0;
  numberOfLeftClicked : number = 0;


  constructor( private matchService : MatchService ) {

    for(let i=0; i<10; i++){
      this.leftButtonClass[i] = 'ui olive button';
      this.leftButtonsDisabled[i] = false;
      this.rightButtonsDisabled[i] = true;
      this.leftButtonsDisabledCheck[i] = false;
      this.rightButtonsDisabledCheck[i] = false;
    }
  }

  @Input() shouldStartTimer : boolean;
  ngOnInit(): void {
    this.shouldStartTimer = false;
    this.getPairs();
    $("#matchProgressBarID").progress({percent: 100});

  }

  ngOnChanges() {
    if (this.shouldStartTimer) {
      this.startTimer(this.matchPairTimer, this);
    }
  }


  setPairsIntoButtons(_id : string, pairs : any[]) : void {

    this.subject = _id;

    for(let i = 0; i < 10; i++) {    
      const key = Object.keys(pairs[i]);
      this.firstColumn.push(key[0]);
      this.secondColumn.push(pairs[i][key[0]]);
    }

    this.secondColumn = shuffle(this.secondColumn);
    this.firstColumn = shuffle(this.firstColumn);
  }

  getPairs() {
    const sub = this.matchService.sendPairs().subscribe((response) =>{

      this.setPairsIntoButtons(response._id, response.pairs);

      this.pairs = response['pairs'];
    });
  }

  onLeftButtonClick(event: Event){

    this.numberOfLeftClicked++;

    for(let i=0; i<10; i++){
      if(!this.leftButtonsDisabledCheck[i]){
        this.leftButtonsDisabled[i] = true;
      }
     
      if(!this.rightButtonsDisabledCheck[i]){
        this.rightButtonsDisabled[i] = false;

      }
    }


    this.leftButtonClickedId =Number(((event.target as HTMLButtonElement).id));

  }

  onRightButtonClick(event: Event){

    for(let i=0; i<10; i++){
      if(!this.leftButtonsDisabledCheck[i]){
        this.leftButtonsDisabled[i] = false;
      }
     
      if(!this.rightButtonsDisabledCheck[i]){
        this.rightButtonsDisabled[i] = true;

      }
    
    }
    this.rightButtonClickedId =Number(((event.target as HTMLButtonElement).id));

    var num = new Number(this.leftButtonClickedId); 
    var leftButtonId = num.toString();
    var leftButton = <HTMLInputElement>document.getElementById(leftButtonId);
    

    if(this.checkIfPair(this.leftButtonClickedId, this.rightButtonClickedId)){
      // correct match
      this.pointsWon += 2;

      (event.target as HTMLButtonElement).className = 'ui medium green button';
      (event.target as HTMLButtonElement).disabled = true;
      this.leftButtonClass[this.leftButtonClickedId] = 'ui medium green button';
      this.leftButtonsDisabled[this.leftButtonClickedId] = true;
      this.leftButtonsDisabledCheck[this.leftButtonClickedId] = true;
      this.rightButtonsDisabledCheck[this.rightButtonClickedId]  = true;
      leftButton.disabled = true;

      if(this.numberOfLeftClicked === 10){
        this.stopTimer();
        this.nextGameDisabled=false;
      }
     
    }

    else{
      this.leftButtonClass[this.leftButtonClickedId] = 'ui medium red button';
      this.leftButtonsDisabledCheck[this.leftButtonClickedId] = true;
      this.leftButtonsDisabled[this.leftButtonClickedId] = true;

      leftButton.disabled = true;

      if(this.numberOfLeftClicked === 10){
        this.stopTimer();
        this.nextGameDisabled=false;
      }
    }

    

    
  }

  checkIfPair(leftId : number, rightId : number){
    
    let keyClicked = this.firstColumn[leftId];
    let valueClicked = this.secondColumn[rightId];
    var trueValue;

    for(let i=0; i<10; i++){
      if(Object.keys(this.pairs[i])[0] === keyClicked){
        trueValue = Object.values(this.pairs[i])[0];
        break;
      }
    }

    if(valueClicked === trueValue){
      return true;
    }

    return false;
  }
  ngAfterViewInit(): void {

  }

  noMoreTime(){
    for(let i=0; i<10; i++){
      this.leftButtonClass[i] = 'ui olive button';
      this.leftButtonsDisabled[i] = true;
      this.rightButtonsDisabled[i] = true;
      this.nextGameDisabled = false;
    }
  }

  @Output() showNextGame : EventEmitter<boolean> = new EventEmitter<boolean>();
  onNextGame() {
    this.sendPointsWon.emit(this.pointsWon);
    this.showNextGame.emit(true);
  }

}
