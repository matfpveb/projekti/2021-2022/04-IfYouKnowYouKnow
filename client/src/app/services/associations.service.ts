import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class AssociationsService {
    private response: Observable<Object>;
    private readonly _url: string = 'http://localhost:5000/games/associations/sendAssociation';

    public constructor (private http: HttpClient) {}

    public sendAssociation() : Observable<Object> {
        this.response = this.http.get<Object>(this._url, {
            observe: 'body',
            responseType: 'json'
        });

        return this.response;
    }
};