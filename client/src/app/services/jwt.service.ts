import { Injectable } from '@angular/core';
import { IJwtTokenData } from '../models/jwt-token-data';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  private static USER_TOKEN_ID = "USER_JWT_TOKEN";

  constructor() { }

  public setToken(jwt: string): void {
    localStorage.setItem(JwtService.USER_TOKEN_ID, jwt);
  }

  public getToken(): string {
    const token: string|null = localStorage.getItem(JwtService.USER_TOKEN_ID)

    if(!token)
      return "";
    else 
      return token;
  }

  public removeToken(): void {
    localStorage.removeItem(JwtService.USER_TOKEN_ID);
  }

  public getDataFromToken():IJwtTokenData | null {
    const token = this.getToken();

    if (!this.getToken())
      return null;
      
    const payloadString: string = token.split('.')[1]
    const userDataJson: string = atob(payloadString) // desifroanje

    return JSON.parse(userDataJson);
  }
}
