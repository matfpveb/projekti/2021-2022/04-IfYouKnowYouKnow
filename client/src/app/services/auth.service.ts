import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { data } from 'jquery';
import { map, Observable, Subject, tap } from 'rxjs';
import { IJwtTokenData } from '../models/jwt-token-data';
import { User } from '../models/user.model';
import { JwtService } from './jwt.service';

declare const $: any;
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly urls = {
    signUp: "http://localhost:5000/login/signup",
    signIn: "http://localhost:5000/login/signin",
    addGame: "http://localhost:5000/login/game"
  }

  private readonly userSubject: Subject<User | null> = new Subject<User | null>();
  public readonly user: Observable<User|null> = this.userSubject.asObservable(); // regiter when subject changes

  constructor(private http: HttpClient, 
    private jwt: JwtService) { }

  public registerUser(username: string, password:string, email:string) : Observable<User | null>{
    const body = {
      username,
      email,
      password
    }
    // const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    const obs: Observable<{token:string}> = this.http.post<{token:string}>(this.urls.signUp, body);

    return obs.pipe(
      tap( (response: {token:string}) =>  this.jwt.setToken(response.token)), // sacuvamo token i pustimo da dalje
      map( (response: {token:string}) =>  this.sendUserDataIfExists() )
    )
  }

  public addGame(points:Number): Observable<User | null> {
    const body = {
      username: this.sendUserDataIfExists()?.username,
      points
    }

    console.log(body)

    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    const obs: Observable<{token:string}> = this.http.post<{token:string}>(this.urls.addGame, body, {headers});
    return obs.pipe(
      tap( (response: {token:string}) =>  this.jwt.setToken(response.token)), // sacuvamo token i pustimo da dalje
      map( (response: {token:string}) =>  this.sendUserDataIfExists() )
    )
  }

  public sendUserDataIfExists(): User | null{
    const payload: IJwtTokenData | null = this.jwt.getDataFromToken();

    console.log(payload)

    if (!payload)
      return null;

    const newUser: User = new User(payload.username, payload.email, payload.password, payload.games);
    this.userSubject.next(newUser);
    return newUser
  }

  public login(username:string, password:string): Observable<User | null>{
    const body = {username, password};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    const obs: Observable<{token:string}> = this.http.post<{token:string}>(this.urls.signIn, body, {headers});

    return obs.pipe(
      tap((response: {token:string}) => this.jwt.setToken(response.token)),
      map((response: {token:string}) => this.sendUserDataIfExists())
    )
  }

  
  public logout(): void {
    this.jwt.removeToken();
    this.userSubject.next(null);
  }
}
