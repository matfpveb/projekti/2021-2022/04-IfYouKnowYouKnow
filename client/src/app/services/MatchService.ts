import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { LogService } from "../log.service";
import {Pair} from '../models/pair.model';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root',
})
export class MatchService {
  private response: Observable<Pair>;
  private readonly _url: string = 'http://localhost:5000/games/match/sendPairs';

  public constructor(private http: HttpClient) {}


  public sendPairs() : Observable<Pair>{

    this.response = this.http.get<Pair>(this._url, {
        observe: 'body',
        responseType: 'json'
    });
    return this.response;
  }

  

};