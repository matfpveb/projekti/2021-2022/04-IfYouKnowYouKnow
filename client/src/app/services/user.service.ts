import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, tap } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly urls = {
    patchUser: "http://localhost:5000/login",
  }

  constructor(private http: HttpClient,
      private jwt: JwtService,
      private auth: AuthService) { }

  public patchUserData(username: string): Observable<User> {
    const body = {username};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);
    
  // ovo treba da nam vrati azurirani token
    return this.http.patch<{token: string}>(this.urls.patchUser, body, {headers}).pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.auth.sendUserDataIfExists()!)
      )
  }

    
}
