import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { LogService } from "../log.service";
import {Word} from '../models/jigsaw.model';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root',
})
export class WordsService {
  private response: Observable<Word>;
  private readonly checkWordURL = 'http://localhost:5000/games/words/checkWord';
  private readonly findLongestURL =
    'http://localhost:5000/games/words/findLongest';

  public constructor(private http: HttpClient) {}

  public checkWord(foundWord: string): Observable<Word> {
    const params = new HttpParams().set('word', foundWord);

    this.response = this.http.get<Word>(this.checkWordURL, {
      observe: 'body',
      responseType: 'json',
      params: params,
    });
    return this.response;
  }

  public findLongestWord(selectedLetters: string): Observable<Word> {
    const params = new HttpParams().set('letters', selectedLetters);

    this.response = this.http.get<Word>(this.findLongestURL, {
      observe: 'body',
      responseType: 'json',
      params: params,
    });
    return this.response;
  }
};