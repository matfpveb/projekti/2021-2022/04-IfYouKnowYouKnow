import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const UserPasswordValdator: ValidatorFn = (control: AbstractControl) : ValidationErrors | null => {
    const pass = control.value.split('')

    if (pass.filter((c:string) => {return c===' ' || c==='\n' || c==='\t'}).lengt < 2){
        return {
            userPassword: "Password cannot contain blank spaces!"
        }
    }

    if (pass.filter((c:string) => {return '0' <= c && c <= '9'}).length < 2){
        return {
            userPassword: "Password must contain at least 2 numbers!"
        }
    }

    if (pass.filter((c:string) => {return 'a'<=c && c<='z'}).length < 2){
        return {
            userPassword: "Password must contain at least 2 lower case letters!"
        }
    }

    if (pass.filter((c:string) => {return 'A'<=c && c<='Z'}).length < 2){
        return {
            userPassword: "Password must contain at least 2 upper case letters!"
        }
    }

    return null;
}