'use strict';
const { read } = require('fs');
const mongoose = require('mongoose');
const User = require('../models/user-schema');
const bcrypt = require('bcrypt');
const jwtUtil = require('../utils/jwt')

const hashPassword = async (password) => {
    const SALT_ROUNDS = 10;
    return await bcrypt.hash(password, SALT_ROUNDS);
}

// nonEncryptedPass encrptedPass
const checkPassword = async (oldPass, newPass) => {
    return await bcrypt.compare(oldPass, newPass);
}


async function getUserJWTByUsername(username) {
    const user = await getUserByUsername(username);
    if (!user) {
      throw new Error(`User with username ${username} does not exist!`);
    }

    return jwtUtil.generateJWT({
      username: user._id,
      email: user.email,
      password: user.password,
      games: user.games,
    });
  }


async function getUserByUsername(username) {
    return User.findOne({_id: username}).exec(); 
}

async function getAllUsers() {
    return User.find({}).exec();
}

async function addNewUser(username, email, password){
    const pass = await hashPassword(password);

    const newUser = new User({
        _id: username,
        email: email,
        password: pass,
        userType: 'user',
        games: []
    })

    await newUser.save();
    // return getUserByUsername(username);  
    return getUserJWTByUsername(username);  
}

async function deleteUser(username, password){
    const user = await User.findById(username).exec();

    if (user) {
        const checkPass = await checkPassword(password, user.password); // nonEncrptedPass encrptedPass
        if (!checkPass){
            // res.status(400).json("Wrong password");
            return;
        }
        User.findByIdAndDelete(username).exec()
        return true
    }
    else {
        return false
    }
}

async function addGameForUsername (username, points) {
    await User.updateOne({
        _id: username
        }, 
        {$push: {
            games: {
                points: points,
                date: new Date()
            }
        }
    });
    return getUserJWTByUsername(username);
}



module.exports = {
    getAllUsers,
    getUserByUsername,
    addNewUser,
    deleteUser,
    addGameForUsername,
    getUserJWTByUsername,
    checkPassword
}