'use strict';
const Match = require('../models/match-schema');
const mongoose = require('mongoose');
const { db } = require('../models/match-schema');
const { json } = require('body-parser');


const getPairs = async () => {
	const ids = ["country with capital city",
				"author with book",
				"currency with country",
				"song with singer",
				"movie with director"];

	
	let id = ids[Math.floor(Math.random() * ids.length)];
	const pairs = await Match.findOne({"_id" : id}).exec();

	return pairs;
};

const insertCategory = async (id, pairs1) => {

	//instance modela predstavljaju dokumente
	const newMatch = new Match({
		_id: id,
		pairs: pairs1
	});

	await newMatch.save();

};

const insertAllCategories = async (filename) => {
	const matchCount = await Match.find({}).count().exec();
	if (matchCount === 0) {
		console.log(`Inserting categories initiated. Please wait, this will take a while...`)
		let i = 0;
		let j = 0;
		let patternLen = 10;
		let frequency = 20;
		for (const key of Object.keys(filename)){
			if (++j % frequency === 0){
				i = ++i % patternLen;
			}
			process.stdout.write(`[${" ".repeat(i)}=${" ".repeat(patternLen - i - 1)}]\r`);

			await insertCategory(key, filename[key]);
		}
	}
	console.log(`Inserting categories done. Data ready for use!`)
};


module.exports = {
	getPairs,
	insertCategory,
	insertAllCategories
};