'use strict'
const Association = require('../models/associations-schema');
const mongoose = require('mongoose');
const { db } = require('../models/associations-schema');
const { json } = require('body-parser');
const fs = require('fs');

const loadAssociation = async () => {
    const ids = [
        "association_1",
        "association_2",
        "association_3",
        "association_4",
        "association_5"
    ];

    let id = ids[Math.floor(Math.random() * ids.length)];
    const association = await Association.findOne({"_id" : id}).exec();
    // console.log("ASSOCIATION: " + association);

    return association;
}

const insertAssociation = async (id, association) => {
    const newAssociation = new Association({
        _id: id,
        column_a: association[0]['column_a'],
        column_b: association[1]['column_b'],
        column_c: association[2]['column_c'],
        column_d: association[3]['column_d'],
        main_solution: association[4].main_solution
    });

    // console.log("INSERTING NEW ASSOCIATION");
    // console.log(newAssociation);
    // console.log("FINISHING INSERTION");

    await newAssociation.save();
}

const insertAllAssociations = async (association_list) => {
    await Association.deleteMany({});

    for(const key of Object.keys(association_list)) {
        await insertAssociation(key, association_list[key]);
    }

    console.log("Inserting Associations done. Data ready for use!");
}

module.exports = {
    loadAssociation,
    insertAssociation,
    insertAllAssociations
}
