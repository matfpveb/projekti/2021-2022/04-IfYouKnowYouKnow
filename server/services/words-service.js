"use strict";
const Word = require("../models/word-schema");
const mongoose = require("mongoose");

const getLetterFrequencyMap = (letters) => {
  let map = {};
  for (const l of letters) {
    if (!map.hasOwnProperty(l)) {
      map[l] = 1;
    } else {
      map[l] = map[l] + 1;
    }
  }

  return map;
};

const findWord = async (word) => {
  const wordFound = await Word.findOne({ _id: word }).exec();
  return wordFound;
};

const getRegex = (letterFrequencyMap) => {
  // whenever we find '#', replace it with the number from map that corresponds with that letter, and '~' replace with that letter
  let lookaheadGroup = "(?=(?:[^~]*~){0,#}[^~]*$)".split("");
  let backGroup = "^[";
  let regex = "";
  for (const el of Object.keys(letterFrequencyMap)) {
	  backGroup += el;
	  // no need for a loop when the positions are static
	  let currentRegexPart = lookaheadGroup;
	  currentRegexPart[8] = el;
	  currentRegexPart[11] = el;
	  currentRegexPart[20] = el;
	  currentRegexPart[16] = letterFrequencyMap[el];
	  regex += currentRegexPart.join('');
  }
  regex += backGroup + "]+$";
  return regex;
};

const getLongest = async (regex) => {
  const bestWord = await Word.find({ _id: {$regex: `${regex}`, $options: 'x'}, length: {$lte: 12}}).sort({length: -1}).limit(1).exec();

  return bestWord[0];
};

const insertWord = async (word, length) => {
  const newWord = new Word({
    _id: word,
    length,
  });
  await newWord.save();
};

const insertAllWords = async (filename) => {
  const wordCount = await Word.find({}).count().exec();
  if (wordCount === 0) {
    console.log(
      `Inserting words initiated. Please wait, this will take a while...`
    );
    let i = 0;
    let j = 0;
    let patternLen = 10;
    let frequency = 20;
    for (const key of Object.keys(filename)) {
      if (++j % frequency === 0) {
        i = ++i % patternLen;
      }
      process.stdout.write(
        `[${" ".repeat(i)}=${" ".repeat(patternLen - i - 1)}]\r`
      );

      await insertWord(key, filename[key]);
    }
  }
  console.log(`Inserting words done. Data ready for use!`);
};


module.exports = {
  getLetterFrequencyMap,
  getRegex,
  findWord,
  getLongest,
  insertWord,
  insertAllWords,
};
