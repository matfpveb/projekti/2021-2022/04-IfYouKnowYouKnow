'use strict';
const express = require('express');
const {json, urlencoded} = require('body-parser')
const mongoose = require('mongoose');
const fs = require('fs');
const Word = require('../server/models/word-schema');
const {findWord, insertWord, insertAllWords} = require('./services/words-service');
const { db } = require('../server/models/word-schema');
const Match = require('../server/models/match-schema');
const Association = require('../server/models/associations-schema');
const {getPairs, insertCategory, insertAllCategories} = require('./services/match-pair-service');
const {loadAssociation, insertAssociation, insertAllAssociations} = require('./services/associations-service');
const usersRoutes = require('./routes/login/login')
// const gameRoutes = require('./routes/game/game.js')
const wordRoutes = require('./routes/games/word.js');
const matchRoutes = require('./routes/games/match.js');
const AssocRoutes = require('./routes/games/associations.js');


const server ='mongodb://localhost:27017/';
const databaseName = 'games';

const data = fs.readFileSync("assets/jigsawWords/wordsDictionary.json");
const doc = JSON.parse(data);

const data1 = fs.readFileSync("assets/match-pairs/matchDictionary.json");
const doc1 = JSON.parse(data1);

const data2 = fs.readFileSync("assets/associations/associationsDictionary.json");
const doc2 = JSON.parse(data2);

mongoose
 .connect(server + databaseName, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
 .then(() => {
     console.log(`Connected to database: ${databaseName}`);

     insertAllWords(doc);
     insertAllCategories(doc1);
     insertAllAssociations(doc2);
    })
 .catch(err => console.log(err));


const app = express();


app.use(
  express.urlencoded({
    extended: false,
  })
);
app.use(express.json());


// CORS
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PATCH, PUT, DELETE');

    return res.status(200).json({});
  }

  next();
});

app.use('/games/words', wordRoutes);
app.use('/games/match', matchRoutes);
app.use('/games/associations', AssocRoutes);
app.use('/login', usersRoutes)



app.use(function (req, res, next) {
  const error = new Error('Zahtev nije podrzan od servera');
  error.status = 405;

  next(error);
});


app.use(function (error, req, res, next) {
  console.error(error.stack);

  const statusCode = error.status || 500;
  res.status(statusCode).json({
    message: error.message,
    status: statusCode,
    stack: error.stack,
  });
});

module.exports = app