const mongoose = require('mongoose');

const associationsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.String,

    column_a: {
        r1: mongoose.Schema.Types.String,
        r2: mongoose.Schema.Types.String,
        r3: mongoose.Schema.Types.String,
        r4: mongoose.Schema.Types.String,
        solution: mongoose.Schema.Types.String
    },

    column_b: {
        r1: mongoose.Schema.Types.String,
        r2: mongoose.Schema.Types.String,
        r3: mongoose.Schema.Types.String,
        r4: mongoose.Schema.Types.String,
        solution: mongoose.Schema.Types.String
    },

    column_c: {
        r1: mongoose.Schema.Types.String,
        r2: mongoose.Schema.Types.String,
        r3: mongoose.Schema.Types.String,
        r4: mongoose.Schema.Types.String,
        solution: mongoose.Schema.Types.String
    },

    column_d: {
        r1: mongoose.Schema.Types.String,
        r2: mongoose.Schema.Types.String,
        r3: mongoose.Schema.Types.String,
        r4: mongoose.Schema.Types.String,
        solution: mongoose.Schema.Types.String
    },

    main_solution: mongoose.Schema.Types.String
});

const Association = mongoose.model('Association', associationsSchema);
module.exports = Association;