// mongoimport --jsonArray --db games --collection users --file accounts.json 


const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	//username
	_id: {
		type: mongoose.Schema.Types.String,
		required: true
	},
	password: {
		type: mongoose.Schema.Types.String,
		required: true
	},
	email: {
		type: mongoose.Schema.Types.String,
		required: true
	},
	userType: {
		type: mongoose.Schema.Types.String,
		enum: ['admin', 'user', 'guest'],
		required: false,
		default: 'guest'
	},
	games: [{
		points: {
			type: mongoose.Schema.Types.Number,
			required: false
		},
		date: {
			type: mongoose.Schema.Types.Date,
			required: false
		}
	}]
});

const User = mongoose.model('User', userSchema);
module.exports = User;