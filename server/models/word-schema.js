const mongoose = require('mongoose');

const wordSchema = new mongoose.Schema({

	_id: {
		type: mongoose.Schema.Types.String,
		required: true
	},
	length: {
		type: mongoose.Schema.Types.Number,
		required: true
	}

});

const Word = mongoose.model('Word', wordSchema);
module.exports = Word;