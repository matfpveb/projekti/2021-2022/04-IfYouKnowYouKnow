const mongoose = require('mongoose');

const matchSchema = new mongoose.Schema({

	_id: mongoose.Schema.Types.String,
	pairs: []

});

const Match = mongoose.model('Match', matchSchema);
module.exports = Match;