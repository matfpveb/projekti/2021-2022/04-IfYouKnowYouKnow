const Association = require('../models/associations-schema');
const { loadAssociation } = require('../services/associations-service');

module.exports.sendAssociation = async (req, res, next) => {
    try {
        const association = await loadAssociation();

        res.status(200).json(association);
    }
    catch(error) {
        next(error);
    }
}