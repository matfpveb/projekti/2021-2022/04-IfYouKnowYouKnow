const Match = require('../models/match-schema');
const {getPairs} = require('../services/match-pair-service');

module.exports.sendPairs = async (req, res, next) => {
	try{
		const pairs = await getPairs();
		
		res.status(200).json(pairs);

	}
	catch(error){
		next(error);
	}
};
