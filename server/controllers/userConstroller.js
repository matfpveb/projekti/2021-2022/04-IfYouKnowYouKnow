const Service = require('../services/user-service');
const jwtUtil = require('../utils/jwt')

const getAllUsers = async(req, res, next) => {
    try {
        const allUsers = await Service.getAllUsers();
        res.status(200).json(allUsers);
    } catch (err) {
        next(err);
    }
}

const getUserByUsername  = async(req, res, next) => {
    try {
        // const {username} = req.body;
        const username = req.params.username
        const user = await Service.getUserByUsername(username)
        if (user == null)
            return res.status(404).json({"message":"User doesn't exist"})
        return res.status(200).json(
            {
                username: user._id,
                email: user.email,
                password: user.password,
                games: user.games
            }
        );
    } catch(err){
        next(err);
    }
}

const addNewUser = async (req, res, next) => {
    try {
        const {username, email, password} = req.body;

        if (!username || !email || !password){
            res.status(400).json({"message":"Field missing"});
            return;
        }

        const oldUser = await Service.getUserByUsername(username)
        if (oldUser){
            res.status(401).json({"message":"Username is already taken"});
            return;
        }

        newUser = await Service.addNewUser(username, email, password) // token
        res.status(201).json({
            token: newUser
        });
    } catch (err) {
        next(err);
    }
}

const deleteUser = async (req, res, next) => {
    try {
        const {username, password} = req.body;
        success = await Service.deleteUser(username, password)
        if(success)
            res.status(201).json("user deleted");
        else 
            res.status(401).json("Something went wrong. Possibly wrong passwod.");
    } catch (err) {
        next(err);
    }
}


const addGameForUsername = async (req, res, next) => {
    try{
        // const {username, points} = req.body;
        const {username, points} = req.body;

        // console.log(username, points)

        udpatedUserJwt = await Service.addGameForUsername(username, points)

        res.status(200).json({token:udpatedUserJwt});
    } catch (err) {
        next(err);
    }
}

const loginUser = async (req, res, next) => {
    const {username} = req.body;

    // console.log(username + " " + password)

    try {
        // const jwt = await Service.getUserByUsername(username)
        const jwt = await Service.getUserJWTByUsername(username)

        if(!jwt)
            return res.status(400).json({"message":"User not found"})

        return res.status(201).json({
            token: jwt
        })
    } catch (err) {
        next(err)
    }
}

module.exports = {
    getAllUsers,
    getUserByUsername,
    addNewUser,
    deleteUser,
    addGameForUsername,
    loginUser
}