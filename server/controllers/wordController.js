const Word = require('../models/word-schema');
const { getLetterFrequencyMap, getRegex, findWord, getLongest } = require('../services/words-service');

module.exports.checkWord = async (req, res, next) => {
	const word = req.query.word;

	try {
		if (word === undefined) {
			const error = new Error('Word is missing');
			error.status = 400;
			throw error;
		}

		const wordFound = await findWord(word);
		if (wordFound !== null) {
			res.status(200).json(wordFound);
		}
		else {
			res.status(200).json({"_id": null, "length": 0});
		}
	}
	catch (error) {
		next(error);
	}
};

module.exports.findLongest = async (req, res, next) => {
	const letters = req.query.letters;
	const letterFrequencyMap = getLetterFrequencyMap(letters);
	const regex = getRegex(letterFrequencyMap);

	try {
		if (letters.length !== 12) {
			const error = new Error('Missing letters');
			error.status = 400;
			throw error;
		}

		const longestWord = await getLongest(regex);
		res.status(200).json(longestWord);

	}
	catch (error) {
		next(error);
	}
};
