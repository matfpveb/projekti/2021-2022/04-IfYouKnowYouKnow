const express = require('express');

const router = express.Router();

const controller = require('../../controllers/wordController');

router.get('/checkWord', controller.checkWord);
router.get('/findLongest', controller.findLongest);

module.exports = router;