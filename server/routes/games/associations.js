const express = require('express');

const router = express.Router();

const controller = require('../../controllers/associationsController');

router.get('/sendAssociation', controller.sendAssociation);

module.exports = router;