const express = require('express');

const router = express.Router();

const controller = require('../../controllers/matchController');

router.get('/sendPairs', controller.sendPairs);

module.exports = router;