const express = require('express')
// const usersController = require('../../controllers/game/xxxx');
// const { getUserByUsername, addNewUser } = require('../../models/xxxx');

const controller = require('../../controllers/userConstroller');
const authentication = require('../../utils/authentication')
const router = express.Router();

router.get('/', controller.getAllUsers);
router.get('/:username', controller.getUserByUsername);
router.post('/signup', controller.addNewUser);
router.post('/signin', authentication.canAuthenticate, controller.loginUser)
router.delete('/', controller.deleteUser)
router.post('/game', authentication.isAuthenticated, controller.addGameForUsername)

module.exports = router