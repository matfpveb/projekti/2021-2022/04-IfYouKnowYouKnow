'use strict';

// only needs to be used once
// creates a new words dictionary that conforms to Words schema
// specified in the server's model
const fs = require('fs');

const data = fs.readFileSync('wordsDictionaryRaw.json');

const dictionary = JSON.parse(data.toString());


for (const [key, value] of Object.entries(dictionary)) {
    dictionary[key] = key.length;
}


fs.writeFile("wordsDictionary.json", JSON.stringify(dictionary), function(err) {
    if (err) {
        console.log(err);
    }
});




