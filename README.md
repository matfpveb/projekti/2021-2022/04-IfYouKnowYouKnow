# Project IfYouKnowYouKnow

IfYouKnowYouKnow is an application that lets users play most of the games featured in the very popular Serbian television quiz **Slagalica** such as: Jigsaw, Associations, If You Know, Match pairs and Jumper. The objective is to win as much points as possible. A user can make their personal profile by signing up. The personal profile contains a list of records for every time the user has played the games, while logged in, and a few additional statistics. Each record contains the timestamp and the total points won.

## Demo video

[![Demo](demo.png)](https://drive.google.com/file/d/1_cFRFMhbZEKSELyi7DrpBWUqCNpDf9Ee/view?usp=sharing)

## Prerequisites

- npm
- node
- Angular
- TypeScript, JavaScript
- HTML, CSS
- MongoDB

## Build

### Clone repository

In terminal, navigate to the desired folder, type in:
``` bash
git clone https://gitlab.com/matfpveb/projekti/2021-2022/04-IfYouKnowYouKnow.git 
```
Now there should be the 04-IfYouKnowYouKnow folder which contains folders **server** and **client** and the README.md file.

### Build server

In terminal, navigate to 04-IfYouKnowYouKnow folder, type in:
``` bash
cd server
npm ci
``` 

### Build client

In terminal, navigate to 04-IfYouKnowYouKnow folder, type in:
``` bash
cd client
npm ci
```

## Run

### Start mongod

Open terminal, type in:
``` bash
sudo systemctl start mongod
```

### Run server

In terminal, navigate to 04-IfYouKnowYouKnow folder, type in:
``` bash
cd server
node server.js
```
The first time server is run, it will take a minute until the database is set, that is until the database is created and all necessary data is imported (this happens automatically). The output should look like this:
```
Server runnnig on 5000...
Connected to database: games
Inserting categories done. Data ready for use!
Inserting Associations done. Data ready for use!
Inserting words done. Data ready for use!
```

### Run client

Open another terminal, navigate  to 04-IfYouKnowYouKnow folder, type in:
``` bash
cd client
ng serve --open
```
Client is running on http://localhost:4200/. Visit the link to play if it does not open automatically.

## Database schemas

<table>
<tr>
<th>User</th>
<th>Word</th>
<th>Match</th>
<th>Association</th>
</tr>
<tr>
<td>

 Field          | Type             | Required       | Description                                        |
 ---------------| -----------------|--------------- |----------------------------------------------------|
 _id            | String           | true           | Unique identifier                                  |
 password       | String           | true           | Hashed password                                    |
 email          | String           | true           | User email adress                                  |
 userType       | String           | false          | An enum defining user type (admin, user, guest)    |
 games          | [(Number, Date)] | (false, false) | List of played games and points won in those games |

</td>
<td>

Field           | Type             | Required       | Description                                        |
 ---------------| -----------------|--------------- |----------------------------------------------------|
 _id            | String           | true           | Literally a word string because they are unique    |
 length         | Number           | true           | Length of that word                                |

</td>
<td>

Field           | Type             | Required       | Description                                        |
 ---------------| -----------------|--------------- |----------------------------------------------------|
 _id            | String           | true           | Category name                                      |
 pairs          | [{Key, Value}    | true           | List of 10 pairs in form {Key, Value}              |

</td>

<td>

Field           | Type                                     | Required       |  Description                                          |
 ---------------| ---------------------------------------- |--------------- |-------------------------------------------------------|
 _id            | String                                   | true           | Unique identifier                                     |
 column_a       | (String, String, String, String, String) | true           | Column "a" of Association with 4 hints and solution   |
 column_b       | (String, String, String, String, String) | true           | Column "b" of Association with 4 hints and solution   |
 column_c       | (String, String, String, String, String) | true           | Column "c" of Association with 4 hints and solution   |
 column_d       | (String, String, String, String, String) | true           | Column "d" of Association with 4 hints and solution   |
 main_solution  | String                                   | true           | Main solution of Association                          |


</td>

</table>


## Developers

- [Jelena Bondžić, 131/2018](https://gitlab.com/JelenaBondzic)
- [Dimitrije Stankov, 307/2017](https://gitlab.com/ginger-with-a-soul)
- [Milica Gnjatović, 18/2018](https://gitlab.com/milicagnjatovic18)
- [Bojan Bardžić, 300/2018](https://gitlab.com/BojanBardzic)
- [Tijana Živković, 66/2018](https://gitlab.com/tijanazivkovic)
